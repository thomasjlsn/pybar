'''Main pybar configuration file.'''


# Enable or disable blocks. ################################################

# Backlight brightness.
ENABLE_BACKLIGHT = False

# Battery percentage + charge state.
ENABLE_BATTERY = True

# CPU usage.
ENABLE_CPU = True

# Disk usage per disk partition.
ENABLE_DISKS = True

# Current IP address
ENABLE_IP = True

# Memory usage.
ENABLE_MEMORY = True

# Network usage, up and down.
ENABLE_NETWORK = True

# Number of available updates, *.pacnew files, and orphaned packages.
ENABLE_PACMAN = False

# Current temperature.
ENABLE_WEATHER = True


# Statusbar settings. ######################################################

BLOCK_SEPERATOR = '|'

# Format strings used for blocks
#
# Each placeholder represents:
#   - label             the name of the block (eg. "BAT")
#   - meter/percentage  the value of the block (eg. "50%")
#   - extra info        extra info about the block (eg. "+" if the
#                       battery is charging)
#
BLOCK_FORMAT = '%s: %s %s'
BLOCK_EXTRA_INFO_FORMAT = '(%s)'

# Whether to display percentages as Unicode meters, or as is.
STATUSBAR_METERS = False

# The characters used for make the meters.
# May be one of "ascii", "block", or "dot".
#
#   ascii: "####=-    "
#   block: "█████▋    "
#   dot:   "●●●●●◐○○○○"
#
STATUSBAR_METER_TYPE = 'dot'

# How many characters wide these meters should be.
STATUSBAR_METER_WIDTH = 10


# Modify server behavior. ##################################################

# Maximum number of concurrent connections to the server.
SERVER_MAX_CONNECTIONS = 5

# Maximum amount of data the server should transmit in each response.
# (in bytes)
SERVER_MTU = 256

# Location of the socket.
SERVER_SOCKET = '/tmp/pybar.sock'

# How the server should encode the data.
SERVER_ENCODING = 'utf-8'


# Settings for the date block. #############################################

# How the date / time is formatted.
# See `man date` for all formatting options.
DATE_FORMAT = '%a, %b %e %k:%M'


# Settings for the CPU block. ##############################################

# Threshold for displaying temperature (in °C)
CPU_TEMP_THRESHOLD = 60


# Settings for the disk block. #############################################

# If True, show disk usage as a percentage instead of byte size.
DISK_PERCENT = True


# Settings for the memory block. ###########################################

# Hide the memory block if usage is low (less than halfway to swapping).
MEMORY_HIDE_LOW = True

# If True, show memory usage as a percentage instead of byte size.
MEMORY_PERCENT = False


# Settings for the network block. ##########################################

# Hide the network block when network is idle.
NETWORK_HIDE_IDLE = False


# Settings for the temperature block. ######################################

# Your current location ID.
#
# Find out what it is at https://weather.com/ (this is the website
# used to get the temp). Search for your desired location, the ID
# will be in the resulting url (the long string at the end).
#
TEMPERATURE_LOCATION_ID = 'e60256f3426acd3c1b3380921210fcffeab628a120c41d1de03b59a0f0dd32ad'

# Preferred units to display temperature in.
# May be one of "c", "f", or "both".
# "both" toggles between "c" and "f" every 10 seconds.
TEMPERATURE_UNITS = 'both'
