'''Current IP address.'''

from socket import gethostbyname, getfqdn
from lib_pybar import Block


def ip_address():
    ip = gethostbyname(getfqdn())

    if ip.startswith('127.'):
        return 'offline'

    return ip


def main():
    return Block(
        source=ip_address,
        sleep_ms=2500,
        weight=6,
    )
