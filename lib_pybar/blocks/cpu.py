'''CPU usage in percentage.'''

from psutil import cpu_percent, sensors_temperatures

from lib_pybar import Block, label, meter
from lib_pybar.config import config


def percent():
    percent = cpu_percent()
    sensors = sensors_temperatures()
    readings = tuple(i.current for i in (*sensors['coretemp'], *sensors['acpitz']))
    temperature = int(sum(readings) / len(readings))

    if temperature > config.CPU_TEMP_THRESHOLD:  # degC
        return label('cpu', meter(percent), f'{temperature}°C')
    else:
        return label('cpu', meter(percent)) if percent > 20 else None


def main():
    return Block(
        source=percent,
        sleep_ms=1500,
        weight=80,
    )
