'''Memory usage.'''

from lib_pybar import Block, human_readable, label, meter, readint
from lib_pybar.config import config


def memory_usage():
    swap_percent = 100 - readint('/proc/sys/vm/swappiness')
    mem = {}

    with open('/proc/meminfo', 'r') as f:
        for line in f.readlines():
            line = line.strip().split()

            if len(line) == 3:
                key, val, _ = line
                key = key[:-1]

            elif len(line) == 2:
                key, val = line
                key = key[:-1]

            mem[key] = int(val)

    # memory usage as calculated by `free(1)`
    # https://stackoverflow.com/a/60104833
    total = mem['MemTotal'] * 1024
    used = (mem['MemTotal'] - mem['MemFree'] - mem['Cached'] - mem['SReclaimable'] - mem['Buffers']) * 1024

    percent = (100 / total) * used

    # Don't show memory usage unless we are more than halfway to swapping.
    if percent < (swap_percent / 2) and config.MEMORY_HIDE_LOW:
        return None

    if config.MEMORY_PERCENT:
        usage = meter(percent)
    else:
        usage = human_readable(used)

    if percent > swap_percent:
        return label('ram', usage, 'swap')
    else:
        return label('ram', usage)

    return None


def main():
    return Block(
        source=memory_usage,
        sleep_ms=1000,
        weight=70,
    )
