'''Temperature.'''

from re import search
from time import strftime

from lib_pybar import Block, interruptable_sleep
from lib_pybar.config import config
from requests import RequestException, get

location_id = config.TEMPERATURE_LOCATION_ID
url = f'https://weather.com/weather/today/l/{location_id}'

# Make sure we don't get rate limited
rate_limit = 60 * 10  # seconds


class Temperature:
    was_last_checked = int(strftime('%s'))
    raw = None

    def F(self):
        return (str(self.raw)
                if self.raw is not None else '--')

    def C(self):
        return (str(int((int(self.raw) - 32) * (5 / 9)))
                if self.raw is not None else '--')


temperature = Temperature()


def get_temp():
    now = int(strftime('%s'))

    if now - temperature.was_last_checked > rate_limit or temperature.raw is None:
        try:
            response = get(url)
            temperature.was_last_checked = int(strftime('%s'))

            if response.status_code == 200:
                temperature.raw = (search(
                    r'CurrentConditions--tempValue[^°]*[0-9]+°', response.text)
                    [0].split('>')[1][:-1]
                )

        except RequestException:
            temperature.raw = None
            interruptable_sleep(30_000)
            return None

    if config.TEMPERATURE_UNITS == 'both':
        if int(strftime('%S')[0]) % 2 == 0:
            return f'{temperature.C()}°C'
        return f'{temperature.F()}°F'

    elif config.TEMPERATURE_UNITS == 'f':
        return f'{temperature.F()}°F'

    elif config.TEMPERATURE_UNITS == 'c':
        return f'{temperature.C()}°C'


def main():
    return Block(
        source=get_temp,
        sleep_ms=5000,
        weight=100,
    )


# print(get_temp())
