'''The segments of the status bar.'''

from lib_pybar.blocks.backlight import main as backlight
from lib_pybar.blocks.battery import main as battery
from lib_pybar.blocks.cpu import main as cpu
from lib_pybar.blocks.date import main as date
from lib_pybar.blocks.disks import main as disks
from lib_pybar.blocks.ip import main as ip
from lib_pybar.blocks.memory import main as memory
from lib_pybar.blocks.network import main as network
from lib_pybar.blocks.pacman import main as pacman
from lib_pybar.blocks.weather import main as weather
