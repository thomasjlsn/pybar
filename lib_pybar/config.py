'''Import the global config file.'''

import importlib.util

spec = importlib.util.spec_from_file_location(
    'config',
    '/etc/pybar_config.py'
)

config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)
